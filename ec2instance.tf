# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}


#Terraform Block
terraform {
  required_version = "0.15.3"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "= 3.36.0"
    }
  }
}

#Simple data module for getting current external IP address
module "myip" {
  source  = "4ops/myip/http"
  version = "1.0.0"
}

#EC2 Instance
resource "aws_instance" "EC2instance" {
  ami             = "ami-0d5eff06f840b45e9"
  instance_type   = "t2.micro"
  key_name        = "ec2"
  vpc_security_group_ids = [aws_security_group.SSHRule.id]
  tags = {
    Name = "Task-ec2"
  }
}

#KeyPair
resource "aws_key_pair" "owner" {
  key_name   = "ec2"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDCVlTZ8rrUKN5VLAZZMLa5XTErpITECDqjcDLyU1/Y4TCaty0n5bqbKwMF4O8I22iVEfq3qibWn5OuMOkDg6lbpC6wm9bghCbW1/RXRAC7WLknoa5RCkGz+xd6w/99yOnw0UvJi+2/mAzYB0xm5Of32OVfnCJ5zWUJ13JqLQg/AvyW35joMyjzBKF6hXVUmRl65z7ieCg2tsmWIJfLLULtBl7TUZax8Em00DRQjjo6agC2G0Aey2WD3LBpdAxMoZtrCB0ir4vKygMIfEuBTR/p+5sWFyMDz0WmFA4YXqL6zlgBopds6ZWBND2hVri0m/7+e4AdkGp/RPtOq8dxo9N0RXMo/LoKW5V8cpGj2yDxoDiN2+UPxYPRjrOKPmT9DoQqj+UXFsIL0fcqIidhDkFV2Eq5ekkn9/Ivf1rsGS2qTeD6Wp12wuJ/ek9rMWmfzftHeP54jqud0e6rzAwS7/KCKGPSidTkQwjAna4n6bXxniOeMeh6iXJxcrTXNvtk5C8= Prajwal@DESKTOP-J088C6I"

}

#Security Group
resource "aws_security_group" "SSHRule" {
  name = "EC2_SSH"

  ingress {
    description = "Allow SSH inbound connection"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = formatlist("%s/32", [module.myip.address])
  }
  ingress {
    description = "Allow HTTP inbound connection"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "Allow HTTPS inbound connection"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "All Traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}



